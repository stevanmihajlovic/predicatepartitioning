package common.predicate;

public enum AttributeType {
	
	NUMERIC, TEXT, DATE;
	
}
